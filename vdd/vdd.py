import numpy as np
import scipy.ndimage.measurements as sndm
import itertools as it
from util import *

FCC = np.array([ [0.5,0.5,0], [0.5,0,0.5], [0,0.5,0.5] ])
BCC = np.array([ [0.5,0.5,0.5], [-0.5,0.5,0.5], [-0.5,-0.5,0.5] ]) 

def in_box(pts, min_corner, max_corner):
	""" Determines whether a list of points is in a box defined by min_corner and max_corner. """
	a = np.round(pts,8) >= min_corner
	b = np.round(pts,8) < max_corner
	return np.logical_and( a.all(1), b.all(1))

def fill_box(box, origin, lattice, basis=[[0,0,0]]):
	""" Fills a box with points on a lattice, including basis and origin """
	
	# get the corners of the bounding box
	cx,cy,cz = np.mgrid[0:2, 0:2, 0:2]
	corners = np.vstack((box[0][cx.ravel()], box[1][cy.ravel()], box[2][cz.ravel()])).T

	# determine the corners of the box in lattice space, giving a paralelipiped in lattice space
	lattice_corners = np.dot(corners, np.linalg.pinv(lattice))

	# generate lattice-space bounding box of the paralelipiped
	# add a little extra space just to be on the safe side
	xlo  = np.floor( lattice_corners[:,0].min() - 1)
	xhi  = np.ceil( lattice_corners[:,0].max() + 1)
	ylo  = np.floor( lattice_corners[:,1].min() - 1)
	yhi  = np.ceil(lattice_corners[:,1].max() + 1)
	zlo  = np.floor( lattice_corners[:,2].min() - 1)
	zhi  = np.ceil( lattice_corners[:,2].max() + 1)

	#length of each side of bounding box
	li = int(xhi - xlo)
	lj = int(yhi - ylo)
	lk = int(zhi - zlo)

	# allocate arrays for our lattice points and our real points
	lcs = np.zeros( (li*lj*lk,3) ) 
	pts = np.zeros( (li*lj*lk,3) ) 

	# generate list of lattice points
	i = np.arange(xlo,xhi)
	j = np.arange(ylo,yhi)
	k = np.arange(zlo,zhi)
	lcs[:,0] = i.repeat(lj*lk)
	lcs[:,1] = np.tile( j.repeat(lk), li )
	lcs[:,2] = np.tile(k, li*lj)

	# generate real space points
	pts= np.vstack([np.dot(lcs+delta, lattice) + origin for delta in basis])
	
	#pts = np.dot(lcs, lattice) + origin

	# test whether ponts are in original box
	mask = in_box(pts, corners[0], corners[7])

	# return the points 
	return pts[mask]

def trilinear_interpolate(pts, phi):
	""" Linearly interpolate points in a voxellated space """
	# find the relevant voxels
	voxels = np.floor(pts).astype(int) % phi.shape
	delta = (pts % phi.shape) - voxels
	a = np.array([0,1])
	offsets = np.vstack( (a.repeat(4), np.tile(a.repeat(2),2), np.tile(a,4)) ).T
	result = np.zeros( pts.shape[0] )
	for offset in offsets:
		weights = (1 - offset ) + (2*offset - 1)*delta
		index = get_indices( (voxels+offset)% phi.shape, phi)
		result +=  phi.reshape(-1)[index] * weights[:,0] * weights[:,1] * weights[:,2]
	return result

def get_pf(md_pt, md_origin, md_l, pf_l):
	""" Determine the coordiantes of an MD point in PF space """
	return (pf_l * (md_pt - md_origin) / md_l) % pf_l

def fill_grain(phi, lattice, box, bounding_box, origin=np.array([0.,0.,0.]), cutoff=0.35, tol=0.2, basis=[[0,0,0]]):
	""" Fill a grain """
	md_l = np.diff(box).T
	pf_l = phi.shape

	pts = fill_box(bounding_box, origin, lattice, basis)
	print "Checking Phase Field..."
	pf_pts = get_pf(pts, origin, md_l, pf_l)

	print "Finding Voxels...",
	phi_index = np.round(pf_pts,0).astype(int) % pf_l
	phi_index = phi_index[:,0]*pf_l[1]*pf_l[2] + phi_index[:,1]*pf_l[2] + phi_index[:,2]
	nn_phi = phi.reshape(-1)[ phi_index ]
	mask = nn_phi > cutoff
	print "(%d)" % mask.sum()
	gb_mask = abs(nn_phi - cutoff) < tol

	print "Interpolating... (%d)" % gb_mask.sum()
	gb_pts = pf_pts[gb_mask]
	update = trilinear_interpolate(gb_pts, phi)
	mask[gb_mask] = update > cutoff

	print "Done!"
	return pts[mask]

