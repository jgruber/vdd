from vdd import *
import scipy.ndimage.measurements as sndm
import os
import subprocess

def fill_polycrystalline(meta, data, shape, scale, lattice, conf={}):
	conf = get_defaults(conf)
	md_box = np.zeros( (3,2) )
	md_box[:,1] = scale * shape
	grains = []
	metas = []
	filled = np.zeros(shape)
	ops = [ x for x in meta['variables'] if 'phi' in x]
	for op in ops:
		print "\n -- Processing OP %s -- " % op
		phi = get_phi(op, meta, data)
		if (phi > 0).sum() == 0:
			print "Empty OP"
		else:
			gs, ms, mask = process_op(phi, scale, lattice, orientation, conf)
			grains += gs
			metas += ms
			np.maximum(filled, mask, filled)

	
	print "Empty Voxels: %d" % (filled < conf['op_cutoff']).sum()
	amorphous = []
	if conf['amorphous'] == "Naive":
		amorphous = fill_empty(filled, scale, conf['r0'], conf['op_cutoff'])
	elif conf['amorphous'] == "Torquato":
		amorphous = fill_empty2(filled, scale, conf['r0'], conf['op_cutoff'])
	elif conf['amorphous'] == "Torquato1":
		print "T1"
		amorphous = fill_empty2(filled, scale, conf['r0'], conf['op_cutoff'], 1)
	grains = amorphous + grains

	print "Total Grains: %d" % len(grains) 
	atoms = process_grains(grains, md_box)

	return atoms, metas

def fill_polycrystalline_spparks(phis, scale, lattice, conf):
	conf = get_defaults(conf)
	shape = np.array(phis[0].shape)
	md_box = np.zeros( (3,2) )
	md_box[:,1] = scale * shape
	grains = []
	metas = []
	filled = np.zeros(shape)

	for i, (phi,o) in enumerate(zip(phis, conf['orientations'])):
		print "\n -- Processing OP %s -- " % i
		if (phi > 0).sum() == 0:
			print "Empty OP"
		else:
			atoms, meta = process_grain(phi, scale, lattice, o, conf)
			grains.append(atoms)
			metas.append(meta)

	print "Total Grains: %d" % len(grains) 
	atoms = process_grains(grains, md_box)

	return atoms, metas


def get_defaults(conf):
	""" Apply default values to configuration and sanity check """
	
	if 'op_cutoff' not in conf:
		conf['op_cutoff'] = 0.35

	if 'op_interp_tolerance' not in conf:
		conf['op_interp_tolerance'] = 0.2
	
	if 'amorphous' not in conf:
		conf['amorphous'] = "None"
	else:
		if 'r0' not in conf:
			raise(ValueError("conf['r0'] must be defined if amorphous filling is enabled!"))

	if 'orientations' not in conf:
		conf['orientations'] = None
	
	return conf

def fill_empty(filled, scale, r0, cutoff):
	""" Fill empty voxels with amorphous """
	unfilled = filled < cutoff
	md_box = np.zeros( (3,2) )
	md_box[:,1] = scale
	coords = np.transpose(unfilled.nonzero())
	atoms = np.vstack([fill_amorphous((c-[0.5,0.5,0.5])*scale, scale, r0) for c in coords])
	return [atoms]


# MAX_MISSES = 100 -> n/ntarget = 0.63
# MAX_MISSES = 1000 -> n/ntarget = 0.77 (much slower)
def fill_amorphous(origin, scale, r0, MAX_MISSES=100):
	v = 4./3.*np.pi*r0**3 / scale**3
	n_target = np.ceil(0.5 / v) 
	d = 4*r0**2

	n = 0
	misses = 0
	atoms = np.empty( (n_target, 3) )
	atoms[:] = np.NaN
	while n < n_target and misses < MAX_MISSES:
		test_pt = np.random.uniform(scale, size=3)
		ds = atoms-test_pt
		ds = (ds*ds).sum(axis=1)
		if np.isnan(np.nanmin(ds)) or np.nanmin(ds) > d:
			atoms[n] = test_pt
			n += 1
			misses = 0
		else:
			misses += 1
	return atoms[:n]

def fill_empty2(filled, scale, r0, cutoff, n_templates=100):
	unfilled = filled < cutoff
	coords = np.transpose(unfilled.nonzero())
	origins = (coords - [0.5,0.5,0.5])*scale
	v = 4./3.*np.pi*r0**3 / scale**3
	n_target = np.ceil(0.5 / v)
	templates = []
	while len(templates) < n_templates:
		meta, data = gen_random_packing(n_target)
		if meta['d']*scale > 2*r0*0.9:
			templates.append(data*scale)
		else:
			print "rejected"
	atoms = np.vstack([templates[np.random.randint(len(templates))] + o for o in origins])
	return [atoms]

def gen_random_packing(n_target):
	spheres_input = """
int eventspercycle = 20;  // # events per sphere per cycle
int N = %d;                         // number of spheres
double initialpf = 0.01;                   // initial packing fraction
double maxpf = 0.60;                   // max packing fraction
double temp = 0.2;    // initial temp., use 0 for zero velocities
double growthrate = 0.1;          // growth rate
double maxpressure = 100.;           // max pressure
char* readfile = new                   // if new, creates new packing
char* writefile = write.dat  // after mp2
char* datafile = stats.dat  // data file up to mp2
""" % n_target
	with open("spheres_input","w") as f:
		f.write(spheres_input)
	# run spheres
	#os.system("spheres spheres_input")
	subprocess.call(["spheres","spheres_input"])
	# read output
	meta, data = read_spheres()
	return meta, data

 

def read_spheres(fname="write.dat"):
	meta = {}
	with open(fname) as f:
		meta['dim'] = int(f.readline())
		f.readline()
		meta['n'] = int(f.readline())
		meta['d'] = float(f.readline())
	data = np.loadtxt("write.dat", skiprows=6)
	return meta, data



def process_grains(grains, md_box):
	""" Convert a list of grains (lists of atomic coordinates) to LAMMPS data """

	n = sum([g.shape[0] for g in grains])
	atoms = np.empty( (n,6) )
	atoms[:,0] = np.arange(n) # Atom ID
	atoms[:,1] = 1 # Atom Type
	atoms[:,2:5] = np.vstack(grains) # x y z
	atoms[:,5] = np.hstack([ np.full(g.shape[0], j, dtype=float) for j,g in enumerate(grains)]) # Grain ID

	# Wrap to PBC
	atoms[:,2] = (atoms[:,2] - md_box[0][0]) % (md_box[0][1] - md_box[0][0]) + md_box[0][0]
	atoms[:,3] = (atoms[:,3] - md_box[1][0]) % (md_box[1][1] - md_box[1][0]) + md_box[1][0]
	atoms[:,4] = (atoms[:,4] - md_box[2][0]) % (md_box[2][1] - md_box[2][0]) + md_box[2][0]
	
	return atoms


def process_op(phi, scale, lattice, orientation, conf):
	"""Generate atoms for a given order parameter.

	Arguments:
	  phi -- the order parameter
	  scale -- scale factor between the MD and PF spaces
	  lattice -- the base lattice of this structure

	Returns
	  gs -- list of atom positions for each grain (not wrapped to PBC)
	  os -- euler angles for each grain

	Algortihm:
	  Identify individual grains in this OP
	  For each grain:
	  	Rescale grains OP to [0,1]
	    Create a contiguous represnetaion of the grain
	    Fill with randomly rotated lattice
	"""

	print "Identifying grains..."
	# Seperate grains in this OP
	grains = id_grains(phi)
	print "Found %d grains" % len(grains)
	gs = []
	metas = []
	phi_out = np.zeros(phi.shape)
	for g in grains:
		#Rescale and log grain
		# if g.max() < 1:
		# 	print "Rescaling Grain... (%f)" % g.max()
		# 	g = g/g.max()
		np.maximum(phi_out, g, phi_out)
		atoms, meta = process_grain(phi, scale, lattice, orientation, conf)
		gs.append(atoms)
		metas.append(meta)
	return gs, metas, phi_out

def process_grain(g, scale, lattice, orientation, conf):
		# Check if this grain crosses periodic boundaries
		v = boundary_violations(g)
		
		# Replicate OP in crossed directions
		reps = np.array([1,1,1]) + np.array([2,2,2]) * v
		g2 = np.tile(g, reps)
		
		# Label contiguous regions
		labels = sndm.label(g2)
		
		# Find a voxel within the central cell, now guarunteed to be part of a full, contiguous grain
		pt = np.transpose(g.nonzero())[0] + v * np.array(g.shape)
		
		# Wipe out voxels that don't match our chosen voxel
		l = labels[0][tuple(pt)]
		g2[labels[0] != l] = 0
		
		# Determine the bounding box of our grain in PF space, translate to MD space
		gcs = (labels[0] == l).nonzero()
		bbox = np.array([(gcs[i].min(),gcs[i].max()) for i in [0,1,2] ])
		md_bbox = scale*bbox
		
		# Determine MD box correspoding to replicated OP
		md_box = np.zeros( (3,2) )
		md_box[:,1] = scale*np.array(g2.shape)

		# Generate and log our new lattice

		alpha, beta, gamma = orientation

		new_lattice = rot_x(alpha).dot( rot_y(beta)).dot(rot_z(gamma)).dot(lattice).T


		# Generate atoms (not wrapped to PBC yet)
		atoms = fill_grain(g2, new_lattice, md_box,  md_bbox, cutoff=conf['op_cutoff'], tol=conf['op_interp_tolerance'])
		print "Created %d atoms" % len(atoms)

		# Calculate and Log Metadata
		cent_pf = np.array(sndm.center_of_mass(g2, labels[0], l)) % g.shape
		cent_md = cent_pf * scale
		volume_pf = (g2 > conf['op_cutoff']).sum()
		volume_md = volume_pf * scale**3
		size_md = 2*np.power(volume_md * 3.0/4.0 / np.pi, 1.0/3.0)

		meta = {
			"n_atoms": atoms.shape[0],
			"orientation": (alpha, beta, gamma),
			"centroid_pf": cent_pf,
			"volume_pf": volume_pf,
			"centroid_md": cent_md,
			"size_md": size_md
		}
		return atoms, meta


def id_grains(phi, cutoff=0.1):
	""" Seperate and scale grains in the same OP.

	Arguments:
	  phi -- the order parameter
	  cutoff -- threshold for a voxel to be considered non-zero

	Returns
	  grains -- seperated order parameters for each grain 

	Algortihm:
	  Label contiguous regions where OP is above cutoff
	  For each periodic boundary:
	    Find points where the label does not agree across the boundary and the label on both sides is nonzero
	    Create a set of equivlencies to fix these disagreements
	    Apply equivlencies

	  Return OPs for each unique label
	"""
	
	# Treshold; find contiguous regions
	mask = phi > cutoff
	labels = sndm.label(mask)
	ls = labels[0]

	# Apply PBC on axis 1
	m = np.logical_and( ls[0,:,:] != ls[-1,:,:], ls[0,:,:]*ls[-1,:,:] > 0)
	eqs = set(zip(ls[0,:,:][m], ls[-1,:,:][m]))
	for i,j in eqs:
		ls[ls==i] = j

    # Apply PBC on axis 2
	m = np.logical_and( ls[:,0,:] != ls[:,-1,:], ls[:,0,:]*ls[:,-1,:] > 0)
	eqs = set(zip(ls[:,0,:][m], ls[:,-1,:][m]))
	for i,j in eqs:
		ls[ls==i] = j

    # Apply PBC on axis 3
	m = np.logical_and( ls[:,:,0] != ls[:,:,-1], ls[:,:,0]*ls[:,:,-1] > 0)
	eqs = set(zip(ls[:,:,0][m], ls[:,:,-1][m]))
	for i,j in eqs:
		ls[ls==i] = j

	grains = []
	for l in np.unique(ls[ls.nonzero()]):
		g = np.zeros(phi.shape)
		g[ls==l] = phi[ls==l]

		grains.append(g)
	return grains

def boundary_violations(phi):
	""" Determine which boundaries are crossed by this OP. """
	violations = np.array([False,False,False])
	violations[0] = phi[0,:,:].sum() + phi[-1,:,:].sum() > 0
	violations[1] = phi[:,0,:].sum() + phi[:,-1,:].sum() > 0
	violations[2] = phi[:,:,0].sum() + phi[:,:,-1].sum() > 0
	return violations

