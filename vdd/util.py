import numpy as np
from scipy.ndimage.measurements import label
from skimage.filters import gaussian

def load_tec(datafile):
	meta = {}
	with open(datafile) as f:
		l = f.readline()
		meta['title'] = l[8:].replace("\""," ").strip()
		l = f.readline()
		meta['variables'] = l[12:].replace(","," ").replace("\""," ").split()
		l = f.readline()
		meta['zone'] = np.array([int(a.strip()[4:]) for a in l[5:].split(",") ])
	data = np.loadtxt(datafile, skiprows=3)
	return meta, data

def load_spparks_txt(datafile):
	meta = {}
	with open(datafile) as f:
		head = f.readline()
	meta['columns'] = head.split(", ")
	data = np.loadtxt(datafile, skiprows=1)
	return meta, data

def rot_x(a):
	s = np.sin(a)
	c = np.cos(a)
	return np.array([[1,0,0],[0,c,s], [0,-s,c]])

def rot_y(a):
	s = np.sin(a)
	c = np.cos(a)
	return np.array([[c,0,-s],[0,1,0], [s,0,c]])

def rot_z(a):
	s = np.sin(a)
	c = np.cos(a)
	return np.array([[c,s,0],[-s,c,0], [0,0,1]])

def rot_xyz(a,b,c):
	return rot_x(a).dot(rot_y(b)).dot(rot_z(c))

def get_indices(f, phi):
	return (f[:,0]*phi.shape[1]*phi.shape[2] + f[:,1]*phi.shape[2] + f[:,2]).astype(int)

def get_phi(op, meta, data):
	phi = np.zeros( meta['zone'] )
	i = meta['variables'].index(op)
	indices = get_indices(data[:,:3],phi)
	phi.reshape(-1)[indices] = data[:,i]
	return phi

def get_voxels_spparks(meta, data):
	shape = tuple(data.ptp(axis=0)[:3].astype(int)+1)
	voxels = np.zeros(shape, dtype=int)
	voxels.reshape(-1)[:] = data[:,-1]
	return voxels


def write_xyz(data):
	with open("out.xyz",'w') as f:
		f.write("%d\n" % len(data))
		f.write("\n")
		for p in data:
			f.write("%e %e %e\n" % (p[0], p[1], p[2]) )

def write_dump(fname, atoms, box):
	natoms = len(atoms)
	with open(fname,"w") as f:
		f.write("ITEM: TIMESTEP\n0\n")
		f.write("ITEM: NUMBER OF ATOMS\n%d\n" % natoms)
		f.write("ITEM: BOX BOUNDS pp pp pp\n%e %e\n%e %e\n %e %e\n" % tuple(box.reshape(6)))
		f.write("ITEM: ATOMS id type x y z grain\n")
		for a in atoms:
			f.write("  %d %d %g %g %g %g\n" % (a[0], a[1], a[2], a[3], a[4], a[5]) )

def boundary_violations(phi):
	""" Determine which boundaries are crossed by this OP. """
	violations = np.array([False,False,False])
	# check if there is a non-zero value on each face
	violations[0] = phi[0,:,:].sum() + phi[-1,:,:].sum() > 0
	violations[1] = phi[:,0,:].sum() + phi[:,-1,:].sum() > 0
	violations[2] = phi[:,:,0].sum() + phi[:,:,-1].sum() > 0
	return violations

def blur_voxels(voxels, sigma=0.5):
	grain_ids = np.unique(voxels)
	ops = []
	for i, grain_id in enumerate(grain_ids):
		print "Blurring grain %d" % i 
		mask = (voxels == grain_id)
		contiguous_mask = make_contiguous(mask)
		phi = gaussian(contiguous_mask, sigma=sigma)
		if phi.shape != mask.shape:
			phi = un_make_contiguous(phi, boundary_violations(mask))
		ops.append(phi)
	return ops

def make_contiguous(phi):
	# IMPORTANT NOTE: SIZE IS NOT THE SAME AS ORIGINAL
	# determine which periodic boundaries this grain crosses
	v = boundary_violations(phi)
	# replicate the mask in those directions
	reps = np.array([1,1,1]) + np.array([2,2,2]) * v
	replicated_phi = np.tile(phi, reps)
	# label contirguous areas using a burn/flood/watershed algorith
	labels = label(replicated_phi)
	# pick a point in the original box
	pt = np.transpose(phi.nonzero())[0] + v * np.array(phi.shape)
	# wipe out other copies of the grain that don't match our point
	l = labels[0][tuple(pt)]
	replicated_phi[labels[0] != l] = 0
	new_phi = replicated_phi	
	return new_phi

def un_make_contiguous(phi, boundary_crossings):
	new_phi = phi.copy()
	for axis in np.where(boundary_crossings)[0]:
		new_phi = sum( np.split(new_phi, 3, axis=axis) )
	return new_phi
