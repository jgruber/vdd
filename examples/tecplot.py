#from voronoi import *

# Add the VDD library to the Python Path
# You can do this more permanently by setting the environment variable PYTHONPATH
# The path should point to the parent of "../vdd"
import sys
sys.path.append("..")

#import necessary libraries
import numpy as np
import vdd
import pprint
import cPickle as pickle

# Load the texplot file
print "Loading Data..."
datafile = "sample_data/poly.tec"
meta, data = vdd.load_tec(datafile)

vols = [ (data[:,i] > 0.5).sum() for i in range(4, len(meta['variables'])) ]
vols = np.array([v for v in vols if v > 0])
rs = np.power( vols / (4./3. * np.pi), 1./3. )
r_tec = rs.mean()

ops = [ x for x in meta['variables'] if 'phi' in x]
grains = sum([ vdd.id_grains(vdd.get_phi(op, meta, data)) for op in ops],[])
vols2 = [ (g > 0.35).sum() for g in grains ]
vols2 = np.array([v for v in vols2 if v > 0])
rs2 = np.power( vols2 / (4./3. * np.pi), 1./3. )
r_tec = rs2.mean()

cutoff = 0.35 # OP Cutoff
a0 = 3.610 # Cu
r0 = a0 * np.sqrt(2) / 2.0 # FCC
base_lattice = a0*vdd.FCC

orientations = np.random.uniform(0,np.pi/2, size=(50,3) )
d_targets_nm = np.array([4.0,8.0])

for d_nm in d_targets_nm:
  print "Target: %f" % d_nm
  # Set up constants
  fname = "dump.Cu.%02dnm.pf" % d_nm # file name

  scale = d_nm*0.5*10 / r_tec

  # Set up the MD simulation box
  pf_shape = meta['zone']
  md_box = np.zeros( (3,2) )
  md_box[:,1] = scale*pf_shape

  # Set up the configuration
  # amorphous: how empty spaces should be filled
  # r0: radius of particles for amorhpous filling
  # orientations: either a list of euler angles or None for random orientations
  it = iter(list(orientations.reshape(1,- 1)[0]))
  conf = {
    'amorphous': "None",
    'r0': r0,
    'orientations': it,
  }

  # Get atomic positions and metadata
  atoms, grain_metas = vdd.fill_polycrystalline(meta, data, pf_shape, scale, base_lattice, conf=conf)

  sizes = [m['size_md'] for m in grain_metas if m['size_md'] > 0]
  grain_size = np.mean(sizes)
  print "Actual: %f" % (grain_size/10)
  n_atoms = sum([m['n_atoms'] for m in grain_metas])

  # log metadata
  with open("%s.meta.p" % fname, 'w') as f:
    pickle.dump(grain_metas, f, protocol=0)

  # write dump
  print "Writing File..."
  vdd.write_dump(fname, atoms, md_box)